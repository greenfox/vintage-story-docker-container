#!/bin/bash


main(){
if [ -f "/SaveData/useThisFolder" ]; then
    mono ./VintagestoryServer.exe --dataPath /SaveData
else
    errorMsg
fi
}

errorMsg(){
cat << EOF
Vintage Story Server Docker Container:
If this is your first time running this container, there are a couple of things you should do. 
1) "mkdir"/"cd" into a folder that you'd like to make the save file location. This folder is about to get flooded with save files, so find a good location.
2) "touch" a file in this folder called "useThisFolder".
3) Run this container again like this (replace $THIS_CONTAINER with whatever you used to run this):
docker run -it -v \$(pwd):/SaveData -u \$(id -u):\$(id -g) -p 42420:42420 --restart unless-stopped \$THIS_CONTAINER
EOF
}

main


