FROM mono

RUN apt-get update && apt-get install -y jq && rm -rf /var/lib/apt/lists/*

#ENV VERSION 1.12.5 #change the below line, this does nothing

WORKDIR /VintageStory
RUN curl $(curl -o /dev/stdout http://api.vintagestory.at/stable.json |tac| jq '."1.12.5".server.urls.local' | tr -d '"') -o file.tar.gz \
    && tar -xf file.tar.gz && rm file.tar.gz && chmod a+x * -R

EXPOSE 42420

COPY ./verifyScript.sh /usr/local/bin/verifyScript
ENTRYPOINT ["/usr/local/bin/verifyScript"]
